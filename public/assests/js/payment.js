$(document).ready(function() {
    $('#calculatePayment').on('click', function(e){
        e.preventDefault();
        resetJsonForm();
        var jsonArray = $.trim($('textarea#jsonArray').val());
        var fieldErrors = '';
        var isValid = true;

        if(jsonArray == null || jsonArray == ""){
            fieldErrors += 'Json array is required.<br>';
            $('.jsonArray').addClass('has-error');
            isValid = false;
        }

        if(isValid == false){
            $('.payment-calculate-form .alert-danger').html(fieldErrors);
            $('.payment-calculate-form .alert-danger').css('display', '');
            $('.payment-calculate-form .alert-danger').fadeIn('slow');
            $('.payment-calculate-form .alert-danger').removeClass('hidden');
        }
        console.log(getCookie('user-token'));

        if(isValid){
            // $(this).attr('disabled', 'disabled');
            $('.loader').removeClass('hidden');
            var dataSet = {
                'jsonArray': $.parseJSON(jsonArray),
                'user_id': getCookie('userId')
            };
            $.ajax({
                url: APP_URL + 'payment/calculate',
                type: 'POST',
                dataType: "json",
                data: dataSet,
                headers: {
                    'X-Auth-Token': getCookie('userToken'),
                },
            }).done(function(data) {

                $('.loader').addClass('hidden');
                if(data.message){
                    console.log(data.data);
                    resetJsonFormFields();

                    $jsonData = $('.json-data');
                    $.each(data.data, function( index, value ) {
                        var label;
                        $.each(value, function(key, price){
                            console.log( 'value' );
                            var amount = parseFloat((price.have_to_pay).toFixed(2)) - parseFloat((price.paid).toFixed(2));
                            $jsonData.append('<div><label for="name">'+ index +' have to pay to '+ key + ' '+  amount +'</label></br>');
                        });

                    });


                    $('.payment-calculate-form .alert-success').html(data.message);
                    $('.payment-calculate-form .alert-success').removeClass('hidden');
                    $('.payment-calculate-form .alert-success').show();
                    $('.payment-calculate-form .alert-success').fadeOut(8000);
                    $('#calculatePayment').removeAttr('disabled');

                }
            }).fail(function(data) {
                $('#calculatePayment').removeAttr('disabled');
                $('.loader').addClass('hidden');
                var errorText = '';
                // if(typeof data.responseJSON.errors.email != "undefined"){
                //     errorText += data.responseJSON.errors.email[0]+"<br>";
                // }

                if(errorText != ''){
                    $('.payment-calculate-form .alert-danger').html(errorText);
                    $('.payment-calculate-form .alert-danger').css('display', '');
                    $('.payment-calculate-form .alert-danger').fadeIn('slow');
                    $('.payment-calculate-form .alert-danger').removeClass('hidden');
                }
            });
        }
    });

    function resetJsonFormFields(){
        $('#jsonArray').val('');
        
    }

    function resetJsonForm(){
        $('.json-data').html('');
        $('.description').removeClass('has-error');
        $('.add-post-form .alert-success').addClass('hidden')
        $('.add-post-form .alert-warning').addClass('hidden')
        $('.add-post-form .alert-danger').addClass('hidden')
        $('.add-post-form .alert-success').text('');
        $('.add-post-form .alert-warning').text('');
        $('.add-post-form .alert-danger').text('');
    }
});
