<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
*/
//disable throttle
\Sentinel::disableCheckpoints();

Route::resource('user', 'API\v1\UserController', ['except' => 'destroy']);
Route::post('user/login', 'API\v1\UserController@userLogin');
Route::post('user/logout', 'API\v1\UserController@logout');

Route::resource('payment', 'API\v1\PaymentController', ['only' => [
	'index'
]]);

Route::get('/', function () {
	return redirect('/payment');
});

Route::get('payment/calculate', 'API\v1\PaymentController@getPayment');
Route::post('payment/calculate', 'API\v1\PaymentController@postPayment');






