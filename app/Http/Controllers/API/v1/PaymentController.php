<?php
/**
 * @file    PostController.php
 *
 * PostController Controller
 *
 * PHP Version 5
 *
 * @author  <Sandun Dissanayake> <sandunkdissanayake@gmail.com>
 *
 */
 
namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Libraries\v1\Helper;
use App\Libraries\v1\ValidationHelper;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class PaymentController extends Controller
{
    //private variables
    private $helper;
    private $validationHelper;

    /**
     * @param Helper $helper
     * @param ValidationHelper $validationHelper
     */
    public function __construct(
        Helper $helper,
        ValidationHelper $validationHelper
    ) {
        $this->helper = $helper;
        $this->validationHelper = $validationHelper;
    }

    /**
     * Get payment
     *
     * @return view
     */
    public function index()
    {

        return view('payment.payment')->with('title', 'JSON App');
    }

    /**
     * Create posts view
     *
     * @return view
     */
    public function getPayment()
    {
        if(Auth::check()){

            return view('payment.payment-create')->with('title', 'Payment Calculate');
        }else{

            return redirect('/');
        }
    }

    /**
     * Get calculated json object
     *
     * @return view
     */
    public function postPayment()
    {
        $paidData = Input::get('jsonArray');
        if(isset($paidData)){

            $payment = [];
            $paid = [];
            $haveToPay = [];
            if(isset($paidData['data']) && !empty($paidData['data'])){
                foreach ($paidData['data'] as $key => $dateData) {
                    $totalMembers = count($dateData['friends']);

                    //paid to calculation
                    foreach ($dateData['friends'] as $key => $friend) {

                        if($friend !== $dateData['paid_by']){

                            $haveToPay[$friend][$dateData['paid_by']][] = $dateData['amount']/$totalMembers;
                            $paid[$dateData['paid_by']][$friend][] = $dateData['amount']/$totalMembers;
 
                        }
                    }
                }           
            }

            foreach ($haveToPay as $p => $pay) {

                foreach ($pay as $v => $price) {
                    if(count($price) > 1){
                        unset($haveToPay[$p][$v]);
                        $haveToPay[$p][$v]['have_to_pay'] = array_sum($price);

                    }else{
                        unset($haveToPay[$p][$v][0]);
                        $haveToPay[$p][$v]['have_to_pay'] = array_sum($price);
                    }
                }
            }

            foreach ($paid as $p => $pay) {

                foreach ($pay as $v => $price) {
                    if(count($price) > 1){
                        unset($paid[$p][$v]);
                        $paid[$p][$v]['paid'] = array_sum($price);

                    }else {
                        unset($paid[$p][$v][0]);
                        $paid[$p][$v]['paid'] = array_sum($price);
                    }
                }
            }

            //get paid and have to paid values
            foreach ($haveToPay as $p => $pay) {

                foreach ($pay as $v => $price) {

                    $paidAmount = $this->getPaidAmount($p, $v, $paid);
                    $haveToPay[$p][$v]['paid'] = $paidAmount;
                }

            }


            // dd($haveToPay);

            return $this->helper
                ->response(200, [
                    'message' => 'You have converted the json data successfully.!',
                    'data' => $haveToPay
                ]);

        } else {
            return $this->helper
                ->response(
                    400,
                    ['message' => 'Something went wrong, Please try again later.']
                );
        }

    }

    private function getPaidAmount($from, $to, $paid){

        $paidAmount = 0; 
        foreach ($paid as $p => $pay) {

            foreach ($pay as $v => $price) {

                if($from === $p && $to === $v){

                    $paidAmount = $paid[$p][$v]['paid'];
                    break;
                }
            }
        }

        return $paidAmount;
    }
}
